package rs.edu.raf.dom1.fakenews.util;

public class SharedPreferenceKeys {
    public static final String USERNAME = "app_username";
    public static final String PASSWORD = "app_password";
    public static final String IS_FAVORITED = "app_is_favorited";
}
