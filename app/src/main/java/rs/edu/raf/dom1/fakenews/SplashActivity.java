package rs.edu.raf.dom1.fakenews;

import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import androidx.appcompat.app.AppCompatActivity;
import rs.edu.raf.dom1.fakenews.util.SharedPreferenceKeys;

import static rs.edu.raf.dom1.fakenews.util.SharedPreferenceKeys.USERNAME;

public class SplashActivity extends AppCompatActivity {

    private static final long SPLASH_DURATION = 1500;
    private static final String SPLASH_IMAGE_URL = "https://picsum.photos/1080/1920/?random";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        init();
    }

    private void init() {
        ImageView splashView = findViewById(R.id.splash_image_view);
        Picasso.get()
                .load(SPLASH_IMAGE_URL)
                .into(splashView, new Callback() {
                    @Override
                    public void onSuccess() {
                        attemptLogin();
                    }

                    @Override
                    public void onError(Exception e) {
                    }
                });
    }

    private void attemptLogin() {
        String username =
                getSharedPreferences(getPackageName(), MODE_PRIVATE)
                        .getString(USERNAME, "");

        Intent intent;
        if (username.equals("")) {
            intent =
                    new Intent(this, LoginActivity.class);
        } else {
            intent =
                    new Intent(this, ArticleActivity.class)
                    .putExtra(SharedPreferenceKeys.USERNAME, username);
        }
        transferToActivity(intent);
    }

    private void transferToActivity(Intent intent) {
        new Handler().postDelayed(() -> {
            startActivity(intent);
            finish();
        }, SPLASH_DURATION);
    }
}
