package rs.edu.raf.dom1.fakenews;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import rs.edu.raf.dom1.fakenews.util.SharedPreferenceKeys;

public class LoginActivity extends AppCompatActivity {

    private EditText mUsernameInput;
    private EditText mPasswordInput;
    private Button mLoginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mUsernameInput = findViewById(R.id.username_input);
        mPasswordInput = findViewById(R.id.password_input);

        mLoginButton = findViewById(R.id.login_button);
        mLoginButton.setOnClickListener(this::attemptLogin);
    }
    private void attemptLogin(View v) {
        String username = mUsernameInput.getText().toString();
        String password = mPasswordInput.getText().toString();

        if (username.equals("")) {
            Toast
                    .makeText(this, R.string.bad_username, Toast.LENGTH_SHORT)
                    .show();
            return;
        }

        if (password.equals("")) {
            Toast
                    .makeText(this, R.string.bad_password, Toast.LENGTH_SHORT)
                    .show();
            return;
        }

        getSharedPreferences(getPackageName(), MODE_PRIVATE)
                .edit()
                .putString(SharedPreferenceKeys.USERNAME, username)
                .putString(SharedPreferenceKeys.PASSWORD, password)
                .apply();

        startActivity(
                new Intent(this, ArticleActivity.class)
                        .putExtra(SharedPreferenceKeys.USERNAME, username)
        );
        finish();
    }

}
