package rs.edu.raf.dom1.fakenews;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import rs.edu.raf.dom1.fakenews.util.SharedPreferenceKeys;

import static rs.edu.raf.dom1.fakenews.util.SharedPreferenceKeys.IS_FAVORITED;

public class ArticleActivity extends AppCompatActivity {

    private TextView articleTextView;
    private TextView greetingTextView;
    private ImageView favoriteImageView;
    private Button favoriteButton;

    private boolean isFavorited;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);
        init();
    }

    private void init() {
        articleTextView = findViewById(R.id.article_text_view);
        greetingTextView = findViewById(R.id.greeting_text_view);
        favoriteImageView = findViewById(R.id.favorite_image_view);
        favoriteButton = findViewById(R.id.favorite_button);

        articleTextView.setMovementMethod(new ScrollingMovementMethod());

        String username = getIntent().getExtras().getString(SharedPreferenceKeys.USERNAME, "");
        if (!username.equals("")) {
            greetingTextView.setText("Hello " + username +
                    ", do you like the article?");
        }

        sharedPreferences = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        isFavorited = sharedPreferences
                        .getBoolean(IS_FAVORITED, false);

        favoriteButton.setOnClickListener(this::handleFavoriteButtonClick);
        applyFavoriteStatus();
    }

    private void handleFavoriteButtonClick(View view) {
        isFavorited = !isFavorited;
        applyFavoriteStatus();
    }

    private void applyFavoriteStatus() {
        sharedPreferences.edit().putBoolean(IS_FAVORITED, isFavorited).apply();

        if (isFavorited) {
            favoriteButton.setText("Unfavorite");
            favoriteImageView.setImageResource(R.drawable.ic_star_black);
        } else {
            favoriteButton.setText("Favorite");
            favoriteImageView.setImageResource(R.drawable.ic_star_border_black_24dp);
        }
    }

}
